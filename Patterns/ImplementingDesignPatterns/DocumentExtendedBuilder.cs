﻿using System;

namespace ImplementingDesignPatterns
{
    public class DocumentExtendedBuilder : IDocumentBuilder
    {
        private Document document;

        public DocumentExtendedBuilder()
        {
            document = new Document();
        }

        public void AddText(string text)
        {
            Console.WriteLine("Добавлен текст: ");
            document.Append($"<r>{text}</r>");
            Console.WriteLine("Выполняем...");
        }

        public void AddImage(string image)
        {
            Console.WriteLine("Добавлено изображение: ");
            document.Append($"<img src=\"{image}\" />");
            Console.WriteLine("Выполняем...");
        }
        
        public Document GetDocument()
        {
            return document;
        }
    }
}