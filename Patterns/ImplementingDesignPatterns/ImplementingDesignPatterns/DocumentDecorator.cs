﻿namespace ImplementingDesignPatterns
{
    public abstract class DocumentDecorator : Document
    {
        protected Document document;

        public DocumentDecorator(Document document)
        {
            this.document = document;
        }

        public override void Append(string content)
        {
            document.Append(content);
        }
        
        public override IIterator CreateIterator()
        {
            return document.CreateIterator();
        }
        public override void InformationAboutDocument()
        {
            document.InformationAboutDocument();
        }
    }
}