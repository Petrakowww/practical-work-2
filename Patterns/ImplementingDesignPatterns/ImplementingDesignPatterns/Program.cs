﻿using System;
using System.Runtime.InteropServices;

namespace ImplementingDesignPatterns
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //Создаем документы по builder,у
            
            Console.WriteLine("Создаём документ: ");
            IDocumentBuilder baseBuilder = new DocumentBaseBuilder();
            baseBuilder.AddText("Hello, world!"); //Заполняем его
            baseBuilder.AddImage("Изображение.png");
            Document baseDocument = baseBuilder.GetDocument();
            IDocumentBuilder infoBuilder = new RichTextDocumentBuilder();
            infoBuilder.AddText("Hello, world!"); //Заполняем его
            infoBuilder.AddImage("Изображение.png");
            Document infoDocument = infoBuilder.GetDocument();
            
            // Вывод информации о документе (для проверки)
            
            baseDocument.InformationAboutDocument();
            infoDocument.InformationAboutDocument();
            Console.WriteLine("\n");
            
            // Декорирование документа
            
            Document documentDecoratorBold = new BoldDocumentDecorator(baseDocument);
            documentDecoratorBold.Append("Hello, world!!!");
            documentDecoratorBold.InformationAboutDocument();
            Console.WriteLine("\n");
            Document documentDecoratorItalic = new ItalicDocumentDecorator(infoDocument);
            documentDecoratorItalic.Append("Test");
            documentDecoratorItalic.InformationAboutDocument();
            Console.WriteLine("\n");
            
            // Итерация по содержимому документа (Decorator при этом расширил Document)
            
            IIterator iteratorBase = baseDocument.CreateIterator();
            while (iteratorBase.HasNext())
            {
                string item = iteratorBase.Next();
                Console.WriteLine(item);
            }
            Console.WriteLine("\n");
            
            //Также можем пройтись по декоратору
            
            IIterator iteratorBold = documentDecoratorBold.CreateIterator();
            while (iteratorBold.HasNext())
            {
                string item = iteratorBold.Next();
                Console.WriteLine(item);
            }
            Console.WriteLine("\n");
            
            IIterator iteratorItalic = documentDecoratorItalic.CreateIterator();
            while (iteratorItalic.HasNext())
            {
                string item = iteratorItalic.Next();
                Console.WriteLine(item);
            }
        }
    }
}