﻿using System;
using System.Collections.Generic;

namespace ImplementingDesignPatterns
{
    public class Document
    {
        private List<string> content;

        public Document()
        {
            content = new List<string>();
        }

        public virtual void Append(string content)
        {
            this.content.Add(content);
        }
        
        public List<string> GetContent()
        {
            return content;
        }

        public virtual IIterator CreateIterator()
        {
            return new DocumentIterator(this);
        }

        public virtual void InformationAboutDocument()
        {
            foreach (string line in content)
            {
                Console.WriteLine(line);
            }
        }
    }
}