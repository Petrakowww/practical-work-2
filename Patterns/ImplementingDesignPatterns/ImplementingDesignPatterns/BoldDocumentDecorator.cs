﻿using System;

namespace ImplementingDesignPatterns
{
    public class BoldDocumentDecorator : DocumentDecorator
    {
        public BoldDocumentDecorator(Document document) : base(document) { }

        public override void Append(string content)
        {
            base.Append($"<b>{content}</b>");
        }

        public override void InformationAboutDocument()
        {
            Console.WriteLine("Bold Document Information:");
            base.InformationAboutDocument();
        }
    }
}