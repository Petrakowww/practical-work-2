﻿using System;
using System.Collections.Generic;

namespace ImplementingDesignPatterns
{
    public class DocumentIterator : IIterator
    {
        private List<string> content;
        private int position;
        public DocumentIterator(Document document)
        {
            this.content = document.GetContent();
            position = 0;
        }

        public bool HasNext()
        {
            return position < content.Count;
        }

        public string Next()
        {
            Console.Write($"Строка документа {position}: ");
            string item = content[position];
            position++;
            return item;
        }
    }
}