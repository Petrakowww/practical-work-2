﻿namespace ImplementingDesignPatterns
{
    public class RichTextDocumentBuilder : IDocumentBuilder
    {
        private Document document;

        public RichTextDocumentBuilder()
        {
            document = new Document();
        }

        public void AddText(string text)
        {
            document.Append($"<r>{text}</r>");
        }

        public void AddImage(string image)
        {
            document.Append($"<img src=\"{image}\" />");
        }
        
        public Document GetDocument()
        {
            return document;
        }
    }
}