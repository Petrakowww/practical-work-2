﻿namespace ImplementingDesignPatterns
{
    public interface IDocumentBuilder
    {
        void AddText(string text);
        void AddImage(string image);
        Document GetDocument();
    }
}