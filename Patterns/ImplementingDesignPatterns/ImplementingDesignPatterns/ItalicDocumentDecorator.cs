﻿using System;

namespace ImplementingDesignPatterns
{
    public class ItalicDocumentDecorator : DocumentDecorator
    {
        public ItalicDocumentDecorator(Document document) : base(document) { }

        public override void Append(string content)
        {
            base.Append($"<i>{content}</i>");
        }

        public override void InformationAboutDocument()
        {
            Console.WriteLine("Italic Document Information:");
            base.InformationAboutDocument();
        }
    }
}