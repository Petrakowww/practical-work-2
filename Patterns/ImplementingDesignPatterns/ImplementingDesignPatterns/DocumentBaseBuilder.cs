﻿namespace ImplementingDesignPatterns
{
    public class DocumentBaseBuilder : IDocumentBuilder
    {
        private Document document;

        public DocumentBaseBuilder()
        {
            document = new Document();
        }

        public void AddText(string text)
        {
            document.Append(text);
        }

        public void AddImage(string image)
        {
            document.Append(image);
        }
        
        public Document GetDocument()
        {
            return document;
        }
    }
}