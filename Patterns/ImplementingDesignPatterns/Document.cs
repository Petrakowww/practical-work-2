﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace ImplementingDesignPatterns
{
    public class Document
    {
        private List<string> content;

        public Document()
        {
            content = new List<string>();
        }

        public virtual void Append(string content)
        {
            this.content.Add(content);
        }

        public void DeleteLine(int index)
        {
            if (index >= 0 && index < content.Count)
                this.content.RemoveAt(index);
            else
                Console.WriteLine("Некорректный индекс строки.");
        }

        public void ClearAll()
        {
            while (CreateIterator().HasNext())
            {
                content.RemoveAt(content.Count - 1);
            }
        }
        public List<string> GetContent()
        {
            return content;
        }

        public virtual IIterator CreateIterator()
        {
            return new DocumentIterator(this);
        }

        public virtual void Information()
        {
            CreateIterator().Information();
        }
    }
}