﻿using System;

namespace ImplementingDesignPatterns
{
    public class BoldDocumentDecorator : DocumentDecorator
    {
        public BoldDocumentDecorator(Document document) : base(document) { }

        public override void Append(string content)
        {
            Console.WriteLine("Добавляем жирный шрифт");
            base.Append($"<b>{content}</b>");
        }

        public override void Information()
        {
            Console.WriteLine("Информация декоратора Bold: ");
            base.Information();
        }
    }
}