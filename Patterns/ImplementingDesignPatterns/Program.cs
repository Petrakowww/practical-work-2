﻿﻿using System;

namespace ImplementingDesignPatterns
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //Создаем документы по builder,у
            
            Console.WriteLine("Создаём документ: ");
            IDocumentBuilder baseBuilder = new DocumentBaseBuilder();
            baseBuilder.AddText("Hello, world!"); //Заполняем его
            baseBuilder.AddImage("Изображение.png");
            Document baseDocument = baseBuilder.GetDocument();
            IDocumentBuilder infoBuilder = new DocumentExtendedBuilder();
            infoBuilder.AddText("Hello, world!"); //Заполняем его
            infoBuilder.AddImage("Изображение.png");
            Document infoDocument = infoBuilder.GetDocument();
            
            Console.WriteLine("\n");
            
            //Информация о документах
            baseDocument.Information();
            infoDocument.Information();
            // Декорирование документа
            
            Document documentDecoratorBold = new BoldDocumentDecorator(baseDocument);
            documentDecoratorBold.Append("Hello, world!!!");
            documentDecoratorBold.Information();
            Console.WriteLine("\n");
            Document documentDecoratorItalic = new ItalicDocumentDecorator(infoDocument);
            documentDecoratorItalic.Append("Test");
            Console.WriteLine("\n");
            
            // Итерация по содержимому документа (Decorator при этом расширил Document)
            
            IIterator iteratorBase = baseDocument.CreateIterator();

            while (iteratorBase.HasNext())
            {
                string item = iteratorBase.Next();
                Console.WriteLine(item);
            }
            Console.WriteLine("\n");
            
            //Также можем пройтись по декоратору
            
            IIterator iteratorBold = documentDecoratorBold.CreateIterator();
            iteratorBold.Information();
            Console.WriteLine("\n");
            
            IIterator iteratorItalic = documentDecoratorItalic.CreateIterator();
            iteratorItalic.Information();
        }
    }
}
