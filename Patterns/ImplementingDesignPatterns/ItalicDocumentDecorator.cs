﻿using System;

namespace ImplementingDesignPatterns
{
    public class ItalicDocumentDecorator : DocumentDecorator
    {
        public ItalicDocumentDecorator(Document document) : base(document) { }

        public override void Append(string content)
        {
            Console.WriteLine("Добавляем курсивный шрифт");
            base.Append($"<i>{content}</i>");
        }
        
        public override void Information()
        {
            Console.WriteLine("Информация декоратора Italic: ");
            base.Information();
        }
    }
}