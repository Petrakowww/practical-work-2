﻿namespace ImplementingDesignPatterns
{
    public interface IIterator
    {
        bool HasNext();
        string Next();
        void Information();
    }
}